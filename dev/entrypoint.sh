#!/bin/bash
# Docker entrypoint script.

set -e
# Ensure the app's dependencies are installed
mix deps.get && mix deps.compile && mix phx.digest

exec elixir -S mix phx.server