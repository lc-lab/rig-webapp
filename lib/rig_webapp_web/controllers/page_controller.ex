defmodule RigWebappWeb.PageController do
  use RigWebappWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
