this is a testing example for a webapp served through traefik and RIG

1. clone
2. docker network create net_ext__traefik net_ext__confluent net_ext__rig
3. docker-compose up

everything should be fine!

It fires up a Traefik for routing and load balancing, RIG to manage all incoming requests, kafka + zookeeper and an elixir/phoenix project created with phx.new --no-ecto

The webapp is available with rig.localhost (excluded https stuff in this example so you get a warning)
-> no assets are served

When exposing port 4000 of the playground-phoenix container and make a request to localhost:4000 you can see that the assets are served correctly.